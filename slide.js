// create variables
let i = 0;
let images = [];
let time = 3000;

//create image list
images[0] = 'images/image1.jpg'
images[1] = 'images/image2.png'
images[2] = 'images/image3.jpg'
images[3] = 'images/image4.jpg'

//create function
function imageSlide(){
  document.slide.src = images[i]
  if(i < images.length -1){
    i++;
  } 
  else {
    i = 0;
  }

  setTimeout('imageSlide()', time)
}

window.onload = imageSlide;